package main

import (
	"bitbucket.org/Polkisss/amigobot/modules"
	"bitbucket.org/Polkisss/amigobot/shared"
	"errors"
	"flag"
	"github.com/bwmarrin/discordgo"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func init() {

	flag.StringVar(&shared.Token, "t", "", "Discord Token")
	flag.StringVar(&shared.SystemPrefix, "p", "!", "Bot Prefix")
	flag.Parse()

	if shared.Token == "" {
		err := errors.New("init: no token passed")
		panic(err)
	}
}

func OpenSession() (dg *discordgo.Session) {

	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + shared.Token)
	if err != nil {
		log.Println("error creating Discord session,", err)
		return
	}

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		log.Println("error opening connection,", err)
		return
	}
	return dg
}

func SessionWaitForEnd() {
	// Wait here until CTRL-C or other term signal is received.
	log.Println("Bot is now running. Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

func main() {
	dg := OpenSession()

	for _, mod := range modules.Modules {
		modules.Map[mod.Name] = mod
	}

	dg.AddHandler(modules.Handler)

	SessionWaitForEnd()

	// Cleanly close down the Discord session.
	err := dg.Close()
	if err != nil {
		log.Println(err)
	}
}
