package utils

func StringInsideOf(item string, slice []string) bool {
	for _, elem := range slice {
		if item == elem {
			return true
		}
	}
	return false
}

func StringMap(vs []string, f func(string) string) []string {
	vsm := make([]string, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}
