package utils

import (
	"encoding/csv"
	"strings"
)

func ExtractCVS(a string, comma rune) ([]string, error) {
	r := csv.NewReader(strings.NewReader(a))
	r.Comma = comma
	return r.Read()
}
