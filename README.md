## Amigo Discord Bot
An Discord bot written in pure Go

## Usage
In any platform:
```
$ go build
$ ./amigobot --help
Usage of amigobot:
  -p string
        Bot Prefix (default "!")
  -t string
        Discord Token
```