package embed

import (
	"github.com/bwmarrin/discordgo"
	"time"
)

type Embed struct {
	e *discordgo.MessageEmbed
}

func New() *Embed {
	return &Embed{e: &discordgo.MessageEmbed{}}
}

func (s *Embed) Embed() *discordgo.MessageEmbed {
	s.e.Timestamp = time.Now().Format(time.RFC3339)
	return s.e
}

func (s *Embed) SetTitle(title string) {
	s.e.Title = title
}

func (s *Embed) SetDescription(desc string) {
	s.e.Description = desc
}

func (s *Embed) SetImage(url string) {
	s.e.Image = &discordgo.MessageEmbedImage{URL: url}
}

func (s *Embed) SetThumbnail(url string) {
	s.e.Thumbnail = &discordgo.MessageEmbedThumbnail{URL: url}
}

func (s *Embed) SetColor(color int) {
	s.e.Color = color
}

func (s *Embed) SetAuthor(author *discordgo.User) {
	s.e.Author = &discordgo.MessageEmbedAuthor{
		Name:    author.Username,
		IconURL: author.AvatarURL("16"),
	}
}

func (s *Embed) SetFooter(text, iconURL string) {
	s.e.Footer = &discordgo.MessageEmbedFooter{Text: text, IconURL: iconURL}
}

func (s *Embed) AddField(name, value string) {
	s.e.Fields = append(s.e.Fields, &discordgo.MessageEmbedField{Name: name, Value: value, Inline: false})
}
