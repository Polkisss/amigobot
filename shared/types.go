package shared

import "github.com/bwmarrin/discordgo"

type DiscordModule struct {
	Name             string
	ShortDescription string
	Description      string
	Arguments        map[string]string
	Aliases          []string
	Main             func(s *discordgo.Session, m *discordgo.MessageCreate, mods map[string]DiscordModule, args []string) (err error)
}
