package helpformatter

import (
	"bitbucket.org/Polkisss/amigobot/shared"
	"bitbucket.org/Polkisss/amigobot/shared/embed"
	"bitbucket.org/Polkisss/amigobot/utils"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"math/rand"
	"strings"
	"time"
)

var (
	Name             = "Help"
	ShortDescription = "show help"
	Description      = "shows list of aviable commands"
	Arguments        = map[string]string{
		"-command": "a command to show",
	} // when +, arg is pos. when -, its not.
	Aliases = []string{
		"h",
		"help",
		"usage",
	}
)

func New() (pp shared.DiscordModule) {
	return shared.DiscordModule{Name, ShortDescription, Description, Arguments, Aliases, Main}
}

func formatCommandArguments(toParse map[string]string, mod shared.DiscordModule) (string, error) {
	// !["ping" "Ping"] <enable-timeout>
	// where
	// * <enable-timeout>: test

	firstLine := fmt.Sprintf("%s%q ", shared.SystemPrefix, append(mod.Aliases, mod.Name))
	argsDescription := "\nwhere\n"

	for arg, help := range toParse {
		positional := arg[0] == '+'
		if !positional {
			arg = "<" + arg[1:] + ">"
			argsDescription += fmt.Sprintf("* %s: %s\n", arg, help)
		} else {
			arg = " " + arg[1:]
			argsDescription += fmt.Sprintf("* %s (positional):  %s\n", arg, help)
		}
		firstLine += arg
	}

	out := firstLine + argsDescription

	//log.Println(out)
	return out, nil
}

func Main(s *discordgo.Session, m *discordgo.MessageCreate, mods map[string]shared.DiscordModule, args []string) (err error) {
	rand.Seed(time.Now().Unix())

	// building embed
	e := embed.New()
	e.SetColor(rand.Intn(0xffffff))
	e.SetAuthor(m.Author)

	// help for command
	var commandHelp bool
	for _, mod := range mods {
		if len(args) > 1 && utils.StringInsideOf(strings.ToLower(args[1]), append(mod.Aliases, Name)) {
			e.AddField(fmt.Sprintf("What does %s:", mod.Name), mod.Description)

			how2use, err := formatCommandArguments(mod.Arguments, mod)
			e.AddField(fmt.Sprintf("How to use %s:", mod.Name), how2use)
			if err != nil {
				return err
			}
			commandHelp = true
			break
		}
	}
	// common short help
	if !commandHelp {
		for prefix, mod := range mods {
			e.AddField(prefix,
				fmt.Sprintf("%q - %s", mod.Aliases, mod.ShortDescription))
		}
		e.SetTitle("**Help**")
		e.SetDescription(fmt.Sprintf("type `%s%s <command>` to view help for command", shared.SystemPrefix,
			append(Aliases, Name)[rand.Intn(len(Aliases))]))
	}

	_, err = s.ChannelMessageSendEmbed(m.ChannelID, e.Embed())
	return err
}
