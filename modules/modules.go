package modules

import (
	"bitbucket.org/Polkisss/amigobot/modules/helpformatter"
	"bitbucket.org/Polkisss/amigobot/modules/pingpong"
	"bitbucket.org/Polkisss/amigobot/shared"
	"bitbucket.org/Polkisss/amigobot/utils"
	"github.com/bwmarrin/discordgo"
	"log"
	"strings"
)

var (
	Modules = []shared.DiscordModule{
		pingpong.New(),
		helpformatter.New(),
	}
	Map = map[string]shared.DiscordModule{}
)

func Handler(s *discordgo.Session, m *discordgo.MessageCreate) {

	if strings.HasPrefix(strings.ToLower(m.Content), shared.SystemPrefix) {
		m.Content = m.Content[len(shared.SystemPrefix):] // remove SystemPrefix

		args, err := utils.ExtractCVS(m.Content, ' ')

		if len(args) < 1 {
			return
		}

		for _, mod := range Modules {
			if utils.StringInsideOf(strings.ToLower(args[0]), mod.Aliases) || strings.ToLower(args[0]) == mod.Name {
				err = mod.Main(s, m, Map, args)
				if err != nil {
					_, _ = s.ChannelMessageSend(m.ChannelID, err.Error())
					log.Fatalln(err)
				}
				break
			}
		}
		if err != nil {
			_, _ = s.ChannelMessageSend(m.ChannelID, err.Error())
			log.Fatalln(err)
		}
	}

}
