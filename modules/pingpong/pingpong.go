package pingpong

import (
	"bitbucket.org/Polkisss/amigobot/shared"
	"github.com/bwmarrin/discordgo"
)

var (
	Name             = "Ping"
	ShortDescription = "pong!"
	Description      = "just shows it's alive"
	Arguments        = map[string]string{"-enable-timeout": "test"}
	Aliases          = []string{
		"ping",
	}
)

func New() (pp shared.DiscordModule) {
	return shared.DiscordModule{Name, ShortDescription, Description, Arguments, Aliases, Main}
}

func Main(s *discordgo.Session, m *discordgo.MessageCreate, _ map[string]shared.DiscordModule, _ []string) (err error) {

	_, err = s.ChannelMessageSend(m.ChannelID, "pong!")
	return err
}
